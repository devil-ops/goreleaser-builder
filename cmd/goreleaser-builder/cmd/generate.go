/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bytes"
	"os"
	"os/exec"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/goreleaser-builder/builder"
)

// generateCmd represents the generate command
var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Generate a new .goreleaser.yaml file",
	Run: func(cmd *cobra.Command, args []string) {
		skipValidateEnv, err := cmd.Flags().GetBool("skip-env-validation")
		CheckErr(err, "")
		release, err := cmd.Flags().GetBool("release")
		CheckErr(err, "")
		snapshot, err := cmd.Flags().GetBool("snapshot")
		CheckErr(err, "")
		bldr, err := builder.NewBuilderWithCobra(cmd)
		CheckErr(err, "Error getting GitLabURLs")
		defer bldr.Cleanup()

		// Load some vars in to the env if needed
		err = bldr.InitEnv()
		CheckErr(err, "Initing env")

		// Should we continue?
		err = bldr.ValidateEnvs()
		if skipValidateEnv {
			if err != nil {
				log.Warn().Err(err).Msg("Error Validating Env Vars")
			}
		} else {
			if err != nil {
				log.Fatal().Err(err).Msg("Error Validating Env Vars")
			}
		}

		err = bldr.Fprint(os.Stdout)
		CheckErr(err, "")

		if release && skipValidateEnv {
			log.Warn().Msg("Both 'release' and 'skip-validate-env' have been specified...this will probably end badly 🚒")
		}
		if release {
			log.Info().Msg("Building release packages")
			gargs := []string{"release", "--rm-dist", "--config", "-"}
			if Verbose {
				gargs = append(gargs, "--debug")
			}
			if snapshot {
				gargs = append(gargs, "--snapshot")
			}
			cmd := exec.Command("goreleaser", gargs...)

			buffer := bytes.Buffer{}
			bldr.Fprint(&buffer)
			cmd.Stdin = &buffer
			cmd.Stderr = os.Stderr

			err = cmd.Run()
			CheckErr(err, "Error running goreleaser release")
		}
	},
}

func init() {
	rootCmd.AddCommand(generateCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	generateCmd.Flags().StringArrayP("config-dir", "c", []string{"."}, "Directory containing configuration settings")
	generateCmd.Flags().Bool("skip-env-validation", false, "Don't validate the environment variables before running")
	generateCmd.Flags().BoolP("release", "r", false, "Run goreleaser releaser after generating the config")
	generateCmd.Flags().BoolP("snapshot", "s", false, "Run goreleaser with --snapshot")
}
