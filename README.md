# Goreleaser Builder

Barely functional goreleaser.yaml generator
Example Usage:

```bash
$ go run ./cmd/goreleaser-builder/ generate -c example-configs/gitlab/goreleaser-config.yaml -c example-configs/overwrite/goreleaser-config.yaml
...
```

See example-configs contents for more info. The basic idea is that multiple
config files will get merged together, so we could have a 'base', then custom
configs to overwrite it. 2 is probably all we need, but you can merge infinitely
now with '-c'

## Setup for your dynamic .goreleaser.yaml

Your project needs to include a configuration file defining a few minimal things about your project. Example:

local-file.yaml

```yaml
meta_packages:
  "goreleaser-builder":
    author: "Drew Stinnett"
    main: "cmd/goreleaser-builder/"
    license: "MIT"
```

Generate your project with:

```bash
$ goreleaser-builder generate -c ./lcoal-file.yaml
...
```

The above config is fine alone, however you may want to share configuration
between projects. For that, you could do something like the following:

shared-file.yaml

```yaml
author: "Drew Stinnett"
license: MIT
```

Your local file:

```yaml
meta_packages:
  "goreleaser-builder":
    main: "cmd/goreleaser-builder/"
```

Combine both in to a valid .goreleaser.yaml

```bash
$ goreleaser-builder generate -c shared-file.yaml -c local-file.yaml
...
```

## Advanced Usage

Instead of using multiple configuration files, you can use the
`GORELEASER_$CONFIG_NAME` environment variable to set values. Pretty sure this
works, but haven't totally tested it yet. It's using the viper `AutomaticEnv`
and `SetEnvPrefix("GORELEASER")` configs