package builder

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"

	"github.com/variantdev/vals"

	"github.com/goreleaser/goreleaser/pkg/config"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v3"
)

type MetaPackage struct {
	Name        string `yaml:"name" mapstructure:"name"`
	Main        string `yaml:"main,omitempty" mapstructure:"main"`
	Author      string `yaml:"author,omitempty" mapstructure:"author"`
	Description string `yaml:"description,omitempty" mapstructure:"description"`
	Binary      string `yaml:"binary,omitempty" mapstructure:"binary"`
	License     string `yaml:"license,omitempty" mapstructure:"license"`
	// Couple homebrew things
	UseHomebrew  bool             `yaml:"use_homebrew,omitempty" mapstructure:"use_homebrew"`
	HomebrewTaps []config.RepoRef `yaml:"homebrew_taps,omitempty" mapstructure:"homebrew_taps"`
	// NFPM Packaging
	NFPMFormats []string     `yaml:"nfpm_formats,omitempty" mapstructure:"nfpm_formats"`
	NFPMOpts    *config.NFPM `yaml:"nfpm_opts,omitempty" mapstructure:"nfpm_opts"`
	// Signing
	UseSigns bool          `yaml:"use_signs,omitempty" mapstructure:"use_signs"`
	Signs    []config.Sign `yaml:"signs,omitempty" mapstructure:"signs"`
	// Uploads - Currently just one location per uploda format...should be something like 'rpm': config.Upload{...}
	NFPMUploads map[string]config.Upload `yaml:"nfpm_uploads,omitempty" mapstructure:"nfpm_uploads"`
}

type Builder struct {
	MetaPackages   []*MetaPackage
	Config         *viper.Viper
	RequiredEnvs   []string
	Project        config.Project
	EnvSecrets     map[string]interface{}
	FileEnvSecrets map[string]interface{}
	VRuntime       *vals.Runtime
}

// GenerateEnvFileSecrets should be used for creating new files from vault
// secret contents. The name of the file will be set to the title of the config
// entry. This is to make things like GPG_KEY_PATH work with minimal
// configuration. Files will be created temp files, and removed after the run is
// complete
func (b *Builder) GenerateEnvFileSecrets() error {
	fileSecrets := b.Config.GetStringMap("secret_files")
	valsRendered, err := b.VRuntime.Eval(map[string]interface{}{
		"from_config": fileSecrets,
	})
	if err != nil {
		return err
	}

	b.FileEnvSecrets = map[string]interface{}{}
	for _, data := range valsRendered {
		dataK, ok := data.(map[string]interface{})
		if !ok {
			return errors.New("ERRRRRR this shouldn't happen")
		}
		for k, v := range dataK {
			vs, ok := v.(string)
			if !ok {
				return errors.New("Could not convert key value to string")
			}

			// Create a tempfile and store the path
			tmp, err := os.CreateTemp("", "goreleaser-builder-")
			if err != nil {
				return err
			}
			tmp.Write([]byte(vs))
			tmp.Close()
			log.Info().Str("name", strings.ToUpper(k)).Str("path", tmp.Name()).Msg("Created secret file")
			b.FileEnvSecrets[strings.ToUpper(k)] = tmp.Name()
		}

	}
	return nil
}

func (b *Builder) GenerateSBOMs() error {
	b.Project.SBOMs = []config.SBOM{
		{
			Artifacts: "archive",
		},
	}
	return nil
}

func (b *Builder) GenerateEnvSecrets() error {
	envSecrets := b.Config.GetStringMap("secret_envs")
	valsRendered, err := b.VRuntime.Eval(map[string]interface{}{
		"from_config": envSecrets,
	})
	if err != nil {
		return err
	}

	b.EnvSecrets = map[string]interface{}{}
	for _, data := range valsRendered {
		dataK, ok := data.(map[string]interface{})
		if !ok {
			return errors.New("ERRRRRR this shouldn't happen")
		}
		for k, v := range dataK {
			vs, ok := v.(string)
			if !ok {
				return errors.New("Could not convert key value to string")
			}
			b.EnvSecrets[strings.ToUpper(k)] = vs
		}

	}
	return nil
}

func (b *Builder) GenerateBuilds() error {
	ret := []config.Build{}
	for _, meta := range b.MetaPackages {
		b := &config.Build{
			ID:           meta.Name,
			Main:         "./" + strings.TrimPrefix(meta.Main, "./"),
			Goos:         []string{"linux", "windows", "darwin"},
			Goarch:       []string{"amd64", "arm64"},
			ModTimestamp: "{{ .CommitTimestamp }}",
			Binary:       meta.Binary,
		}
		b.Env = []string{"CGO_ENABLED=0"}
		b.Flags = []string{"-trimpath"}
		b.Ldflags = []string{
			"-s -w",
			fmt.Sprintf("-X {{ .ModulePath }}/%v/cmd.version={{ .Tag }}",
				strings.TrimSuffix(strings.TrimPrefix(meta.Main, "./"), "/")),
		}

		ret = append(ret, *b)
	}
	b.Project.Builds = ret
	return nil
}

func (b *Builder) Fprint(o io.Writer) error {
	var bi bytes.Buffer
	ye := yaml.NewEncoder(&bi)
	ye.SetIndent(2) // this is what you're looking for
	err := ye.Encode(&b.Project)
	if err != nil {
		return err
	}
	rawS := bi.String()
	ret := strings.ReplaceAll(rawS, `'{{ .Env.HOMEBREW_TAP_TOKEN }}'`, `"{{ .Env.HOMEBREW_TAP_TOKEN }}"`)
	fmt.Fprint(o, ret)
	return nil
}

func (b *Builder) GenerateNFPMs() error {
	ret := []config.NFPM{}
	for _, meta := range b.MetaPackages {
		if len(meta.NFPMFormats) == 0 {
			log.Debug().Msg("No formats found in meta package, checking global nfpm_formats")
			err := b.Config.UnmarshalKey("nfpm_formats", &meta.NFPMFormats)
			if err != nil {
				return err
			}
		}

		// Skip packages with no nfpm formats
		if len(meta.NFPMFormats) == 0 {
			continue
		}
		if meta.NFPMOpts == nil {
			err := b.Config.UnmarshalKey("nfpm_opts", &meta.NFPMOpts)
			if err != nil {
				return err
			}
		}
		for _, pkgfmt := range meta.NFPMFormats {
			if meta.NFPMOpts.Vendor == "" {
				return errors.New("Must set vendor in nfpm_opts")
			}
			if meta.NFPMOpts.Section == "" {
				return errors.New("Must set section in npfm_opts")
			}
			if meta.NFPMOpts.Priority == "" {
				return errors.New("Must set priority in npfm_opts")
			}

			b.AppendReqEnv("CI_PROJECT_URL")
			pkgfmtC := config.NFPM{
				ID:         pkgfmt,
				License:    meta.License,
				Vendor:     meta.NFPMOpts.Vendor,
				Section:    meta.NFPMOpts.Section,
				Priority:   meta.NFPMOpts.Priority,
				Maintainer: meta.Author,
				Homepage:   os.Getenv("CI_PROJECT_URL"),
				Formats:    []string{pkgfmt},
			}

			if pkgfmt == "rpm" {
				if b.Config.GetBool("sign_rpms") {
					pe := fmt.Sprintf("NFPM_%v_PASSPHRASE", strings.ToUpper(pkgfmtC.ID))
					b.AppendReqEnv(pe)
					b.AppendReqEnv(fmt.Sprintf("GPG_KEY_PATH"))
					pkgfmtC.RPM = config.NFPMRPM{
						Signature: config.NFPMRPMSignature{
							KeyFile: "{{ .Env.GPG_KEY_PATH }}",
						},
					}
				}
			}
			ret = append(ret, pkgfmtC)
		}
		b.Project.NFPMs = ret
	}
	return nil
}

func (b *Builder) GenerateEnv() error {
	env := b.Config.GetStringSlice("env")
	b.Project.Env = env
	return nil
}

func (b *Builder) GenerateArchives() error {
	archives := []config.Archive{
		{
			Format:       "tar.gz",
			NameTemplate: "{{ .ProjectName }}-{{ .Version }}_{{ .Os }}_{{ .Arch }}",
		},
	}
	b.Project.Archives = archives
	return nil
}

func (b *Builder) GenerateBrews() error {
	ret := []config.Homebrew{}
	for _, meta := range b.MetaPackages {
		if meta.UseHomebrew {
			for _, t := range meta.HomebrewTaps {
				b := config.Homebrew{
					Description: meta.Description,
					License:     meta.License,
					Name:        meta.Name,
					Homepage:    os.Getenv("CI_PROJECT_URL"),
					Install:     fmt.Sprintf("bin.install \"%v\"", meta.Binary),
					Tap:         t,
				}
				ret = append(ret, b)
			}
		}
	}
	b.Project.Brews = ret
	return nil
}

func (b *Builder) GenerateChecksum() error {
	r := config.Checksum{
		NameTemplate: "{{ .ProjectName }}-{{ .Version }}_SHA256SUMS",
		Algorithm:    "sha256",
	}
	b.Project.Checksum = r
	return nil
}

func (b *Builder) GenerateGitLabURLs() error {
	c := &config.GitLabURLs{}

	url := os.Getenv("CI_SERVER_URL")
	if url == "" {
		return errors.New("CI_SERVER_URL must be set")
	}
	b.AppendReqEnv("CI_JOB_TOKEN")

	c.API = fmt.Sprintf("%v/api/v4/", os.Getenv("CI_SERVER_URL"))
	c.Download = os.Getenv("CI_SERVER_URL")
	// Always use a job token
	c.UseJobToken = true
	os.Setenv("GITLAB_TOKEN", os.Getenv("CI_JOB_TOKEN"))
	c.UsePackageRegistry = true

	b.Project.GitLabURLs = *c

	return nil
}

func (b *Builder) GenerateSign() error {
	ret := []config.Sign{}
	err := b.Config.UnmarshalKey("signs", &ret)
	if err != nil {
		return err
	}
	b.Project.Signs = ret
	return nil
}

func (b *Builder) GenerateSnapshot() error {
	s := config.Snapshot{
		NameTemplate: "{{ .Tag }}-snapshot",
	}
	b.Project.Snapshot = s
	return nil
}

func (b *Builder) GenerateUploads() error {
	ret := []config.Upload{}
	for _, meta := range b.MetaPackages {
		if meta.NFPMUploads == nil {
			err := b.Config.UnmarshalKey("nfpm_uploads", &meta.NFPMUploads)
			if err != nil {
				return err
			}
		}
		if len(meta.NFPMFormats) == 0 {
			err := b.Config.UnmarshalKey("nfpm_formats", &meta.NFPMFormats)
			if err != nil {
				return err
			}
		}
		// log.Fatal().Interface("meta", meta).Msg("DING")
		for _, pkgfmt := range meta.NFPMFormats {
			if uploadConfig, ok := meta.NFPMUploads[pkgfmt]; ok {
				b.AppendReqEnv(fmt.Sprintf("UPLOAD_%v_SECRET", strings.ToUpper(pkgfmt)))
				u := config.Upload{
					Name:     fmt.Sprintf("%v", pkgfmt),
					Target:   uploadConfig.Target,
					Method:   uploadConfig.Method,
					Mode:     uploadConfig.Mode,
					Username: uploadConfig.Username,
					IDs:      []string{pkgfmt},
				}
				ret = append(ret, u)
			} else {
				log.Info().Str("fmt", pkgfmt).Str("package", meta.Name).Msg("Skipping upload entry because it's upload config is not defined")
			}
		}
	}
	b.Project.Uploads = ret
	return nil
}

func (b *Builder) GenerateProject() error {
	// Archive
	err := b.GenerateArchives()
	if err != nil {
		return err
	}
	// Builds
	err = b.GenerateBuilds()
	if err != nil {
		return err
	}

	// Checksum
	err = b.GenerateChecksum()
	if err != nil {
		return err
	}

	// Environment
	err = b.GenerateEnv()
	if err != nil {
		return err
	}

	// Homebrews
	err = b.GenerateBrews()
	if err != nil {
		return err
	}

	// NFPMs
	err = b.GenerateNFPMs()
	if err != nil {
		return err
	}

	// Uploads
	err = b.GenerateUploads()
	if err != nil {
		return err
	}

	// SBOMs
	err = b.GenerateSBOMs()
	if err != nil {
		return err
	}

	// Signs
	err = b.GenerateSign()
	if err != nil {
		return err
	}

	// Secrets
	err = b.GenerateEnvSecrets()
	if err != nil {
		return err
	}
	err = b.GenerateEnvFileSecrets()
	if err != nil {
		return err
	}

	// Snapshot
	err = b.GenerateSnapshot()
	if err != nil {
		return err
	}

	// Pull in gitlab URLs
	err = b.GenerateGitLabURLs()
	if err != nil {
		return err
	}

	// Check for required environment variables in the output file
	err = b.GenerateReqEnv()
	if err != nil {
		return err
	}
	return nil
}

func (b *Builder) AppendReqEnv(s string) {
	if !ContainsString(b.RequiredEnvs, s) {
		b.RequiredEnvs = append(b.RequiredEnvs, s)
	}
}

func (b *Builder) GenerateReqEnv() error {
	bi, err := yaml.Marshal(b.Project)
	if err != nil {
		return err
	}

	r := regexp.MustCompile(`{{\s+\.Env\.(\S+)\s+}}`)
	matches := r.FindAllStringSubmatch(string(bi), -1)
	for _, v := range matches {
		b.AppendReqEnv(v[1])
	}

	return nil
}

func NewBuilderWithViper(v *viper.Viper) (*Builder, error) {
	var err error
	b := &Builder{
		Config: v,
	}
	b.AppendReqEnv("GITLAB_TOKEN")
	b.MetaPackages, err = GetMetaPackagesWithViper(v)
	if err != nil {
		return nil, err
	}
	b.VRuntime, err = vals.New(vals.Options{
		CacheSize: 256,
	})
	if err != nil {
		return nil, err
	}
	err = b.GenerateProject()
	if err != nil {
		return nil, err
	}
	return b, nil
}

func NewBuilderWithCobra(cmd *cobra.Command) (*Builder, error) {
	configDirs, err := cmd.Flags().GetStringArray("config-dir")
	if err != nil {
		return nil, err
	}
	v, err := CollectConfig(configDirs...)
	if err != nil {
		return nil, err
	}

	log.Debug().Interface("settings", v.AllSettings()).Msg("Using config")

	return NewBuilderWithViper(v)
}

func GetMetaPackagesWithViper(v *viper.Viper) ([]*MetaPackage, error) {
	ret := []*MetaPackage{}
	var rawMetas map[string]*MetaPackage
	err := v.UnmarshalKey("meta_packages", &rawMetas)
	if err != nil {
		return nil, err
	}
	if rawMetas == nil {
		return nil, errors.New("Could not get the raw metadata info")
	}
	for name, meta := range rawMetas {

		meta.Name = name
		if meta.Main == "" {
			return nil, errors.New("Must include 'main'")
		}

		// Binary name
		if meta.Binary == "" {
			meta.Binary = meta.Name
		}

		// Check some default places
		if meta.Description == "" {
			meta.Description = os.Getenv("CI_PROJECT_DESCRIPTION")
			if meta.Description == "" {
				meta.Description = fmt.Sprintf("Golang utility [%v]", meta.Name)
			}
		}

		// Homebrews
		homebrewTaps := []config.RepoRef{}
		if meta.UseHomebrew {
			err := v.UnmarshalKey("homebrew_taps", &homebrewTaps)
			if err != nil {
				return nil, err
			}
		}
		meta.HomebrewTaps = homebrewTaps

		// Handle NFPMOpts
		/*
			if len(meta.NFPMFormats) > 0 {
				if meta.NFPMOpts == nil {
					err := v.UnmarshalKey("nfpm_opts", &meta.NFPMOpts)
					if err != nil {
						return nil, err
					}
				}
				log.Warn().Interface("opts", meta.NFPMOpts).Msg("YOYOYO")
				if meta.NFPMOpts == nil {
					return nil, errors.New("When using nfpms, you must set up an nfpm_opts section with Vendor, Section and Priority")
				}
			}
		*/

		// We NEEEED an author
		if meta.Author == "" {
			meta.Author = v.GetString("author")
			if meta.Author == "" {
				return nil, errors.New("Must specify an author for all projects")
			}
		}

		// We Neeeeed the license too
		if meta.License == "" {
			meta.License = v.GetString("license")
			if meta.License == "" {
				meta.License, err = DetectLicense(".")
				if err != nil {
					return nil, errors.New("Could not auto detect license")
				}
			}

		}
		ret = append(ret, meta)
	}
	return ret, nil
}

func CollectConfig(files ...string) (*viper.Viper, error) {
	v := viper.New()
	log.Info().Str("file", files[0]).Msg("Top level file")
	if strings.HasPrefix(files[0], "http") {
		f, err := downloadFile(files[0])
		if err != nil {
			return nil, err
		}
		v.SetConfigFile(f)
	} else {
		v.SetConfigFile(files[0])
	}
	err := v.ReadInConfig()
	if err != nil {
		return nil, err
	}

	// If given more than 1 dir, merge the new stuff in
	if len(files) > 1 {
		for _, vd := range files[1:] {
			vo := viper.New()
			vo.SetConfigFile(vd)
			vo.SetEnvPrefix("GORELEASER")
			vo.AutomaticEnv()
			log.Info().Str("file", vd).Msg("Child level dir")
			err = vo.ReadInConfig()
			if err != nil {
				return nil, err
			}

			err = v.MergeConfigMap(vo.AllSettings())
			if err != nil {
				return nil, err
			}
		}
	}
	log.Debug().Interface("settings", v.AllSettings()).Msg("Merged configuration result")
	return v, nil
}

func (b *Builder) InitEnv() error {
	log.Info().Msg("Setting up secret env variables if they exist")
	for k, v := range b.EnvSecrets {
		sv, ok := v.(string)
		if ok {
			log.Info().Str("key", k).Msg("Setting secret env")
			os.Setenv(k, sv)
		} else {
			log.Warn().Str("key", k).Msg("Could not declare as a string, skipping")
		}
	}
	log.Info().Msg("Setting up secret file based env variables if they exist")
	for k, v := range b.FileEnvSecrets {
		sv, ok := v.(string)
		if ok {
			log.Info().Str("key", k).Msg("Setting secret env")
			os.Setenv(k, sv)
		} else {
			log.Warn().Str("key", k).Msg("Could not declare as a string, skipping")
		}
	}
	return nil
}

// Remove junk we created and don't want hanging around
func (b *Builder) Cleanup() {
	for _, pathI := range b.FileEnvSecrets {
		path, ok := pathI.(string)
		if !ok {
			log.Warn().Msg("Error converting interface to string when cleanup ran")
			return
		}
		err := os.Remove(path)
		if err != nil {
			log.Warn().Str("path", path).Msg("Error removing file, sure it existed?")
		} else {
			log.Info().Str("path", path).Msg("Cleaned up file")
		}
	}
	// os.Clearenv()
}
