package builder

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestContainsString(t *testing.T) {
	tests := []struct {
		s []string
		e string
		r bool
	}{
		{[]string{"a", "b", "c"}, "a", true},
		{[]string{"a", "b", "c"}, "d", false},
	}

	for _, test := range tests {
		got := ContainsString(test.s, test.e)
		require.Equal(t, test.r, got)
	}
}

/*
func TestValidateEnvs(t *testing.T) {
	tests := map[string]struct {
		requiredEnv []string
		env         map[string]string
		wantErr     string
	}{
		"good": {
			requiredEnv: []string{"FOO"},
			env: map[string]string{
				"FOO": "bar",
			},
			wantErr: "",
		},
		"bad": {
			requiredEnv: []string{"FOO"},
			env: map[string]string{
				"BAZ": "bar",
			},
			wantErr: "Missing required envs: [FOO]",
		},
	}
	for k, tt := range tests {
		os.Clearenv()
		for e, v := range tt.env {
			os.Setenv(e, v)
		}
		err := ValidateEnvs(tt.requiredEnv)
		if tt.wantErr != "" {
			require.Error(t, err, k)
			require.EqualError(t, err, tt.wantErr)
		} else {
			require.NoError(t, err, k)
		}
	}
}
*/
