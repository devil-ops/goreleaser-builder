package builder

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"

	"github.com/rs/zerolog/log"
)

func ContainsString(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

/*
func ValidateEnvs(ss []string) error {
	var missingEnvs []string
	for _, s := range ss {
		if os.Getenv(s) == "" {
			missingEnvs = append(missingEnvs, s)
		}
	}
	if len(missingEnvs) > 0 {
		return fmt.Errorf("Missing required envs: %v", missingEnvs)
	}
	return nil
}
*/

func (b *Builder) ValidateEnvs() error {
	var missingEnvs []string
	for _, k := range b.RequiredEnvs {
		if os.Getenv(k) == "" {
			if _, ok := b.EnvSecrets[k]; !ok {
				missingEnvs = append(missingEnvs, k)
			}
		}
	}
	if len(missingEnvs) > 0 {
		return fmt.Errorf("Missing required envs: %v", missingEnvs)
	}
	return nil
}

func downloadFile(url string) (string, error) {
	// Create the file
	ext := filepath.Ext(url)
	out, err := os.CreateTemp("", fmt.Sprintf("goreleaser-builder-downloaded.*%v", ext))
	if err != nil {
		return "", err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return "", err
	}
	log.Info().Str("url", url).Str("file", out.Name()).Msg("Downloaded file")

	return out.Name(), nil
}
