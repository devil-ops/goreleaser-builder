package builder

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestDetectLicenseMIT(t *testing.T) {
	got, err := DetectLicense("testdata/mit-license")
	require.NoError(t, err)
	require.Equal(t, "MIT", got)
}

func TestDetectLicenseNone(t *testing.T) {
	got, err := DetectLicense("testdata/empty-dir")
	require.Error(t, err)
	require.Empty(t, got)
	require.EqualError(t, err, "could not find any licenses, please specify one in your config")
}

/*
TODO: Figure out how to make this work
func TestDetectLicenseLowConf(t *testing.T) {
	got, err := DetectLicense("testdata/low-confident-license")
	require.Error(t, err)
	require.Empty(t, got)
	require.EqualError(t, err, "got low confident")
}
*/
