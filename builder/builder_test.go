package builder

import (
	"os"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	// teardown()
	os.Exit(code)
}

func setup() {
	os.Clearenv()
	os.Setenv("CI_SERVER_URL", "https://gitlab.example.com")
}

func TestCollectConfig(t *testing.T) {
	got, err := CollectConfig("testdata/parent-config/goreleaser-config.yaml", "testdata/child-config/goreleaser-config.yaml")
	require.NoError(t, err)
	require.NotNil(t, got)

	require.Equal(t, "baz", got.GetString("foo"))
	require.Equal(t, "hello world", got.GetString("greeter"))
}

func TestNewBuilder(t *testing.T) {
	v := viper.New()
	v.Set("meta_packages", goodMetas)
	b, err := NewBuilderWithViper(v)
	require.NoError(t, err)
	require.NotNil(t, b)
}

func TestNewBuilderNFPMs(t *testing.T) {
	v := viper.New()
	nfpmMeta := map[string]interface{}{
		"foo": map[string]interface{}{
			"main":         "./cli",
			"author":       "Antonius Block",
			"license":      "MIT",
			"nfpm_formats": []string{"rpm", "deb"},
		},
	}
	v.Set("meta_packages", nfpmMeta)
	v.Set("nfpm_opts", map[string]interface{}{
		"vendor":   "Hello Moto",
		"priority": "optional",
		"section":  "admin",
	})
	v.Set("nfpm_uploads", map[string]interface{}{
		"rpm": map[string]interface{}{
			"mode": "archive",
		},
	})
	v.Set("secret_files", map[string]interface{}{
		"GPG_KEY_FILE": "fake",
	})
	b, err := NewBuilderWithViper(v)
	require.NoError(t, err)
	require.NotNil(t, b)

	err = b.GenerateProject()
	require.NoError(t, err)
	require.NotNil(t, b.Project)

	b.Cleanup()
}

func TestNewBuilderError(t *testing.T) {
	v := viper.New()
	b, err := NewBuilderWithViper(v)
	require.Error(t, err)
	require.Nil(t, b)
}

/*
func TestGetProjectWithViper(t *testing.T) {
	v := viper.New()
	v.Set("meta_packages", goodMetas)
	got, err := ProjectWithViper(v)
	require.NoError(t, err)
	require.NotNil(t, got)
}
*/

func TestGenerateProjectWithBuilder(t *testing.T) {
	v := viper.New()
	v.Set("meta_packages", goodMetas)
	b, err := NewBuilderWithViper(v)
	require.NoError(t, err)
	require.NotNil(t, b)

	err = b.GenerateProject()
	require.NoError(t, err)
	require.NotNil(t, b.Project)
}

var goodMetas map[string]interface{} = map[string]interface{}{
	"foo": map[string]interface{}{
		"main":    "./cli",
		"author":  "Antonius Block",
		"license": "MIT",
	},
}

func TestGetMetaPackagesWithViper(t *testing.T) {
	v := viper.New()
	v.Set("meta_packages", goodMetas)
	got, err := GetMetaPackagesWithViper(v)
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Greater(t, len(got), 0)
	require.Equal(t, "./cli", got[0].Main)
	require.Equal(t, "Antonius Block", got[0].Author)
}

func TestGetMetaPackagesWithViperMissingMain(t *testing.T) {
	v := viper.New()
	v.Set("meta_packages", map[string]interface{}{
		"foo": map[string]interface{}{
			"author": "Antonius Block",
		},
	})
	got, err := GetMetaPackagesWithViper(v)
	require.Error(t, err)
	require.EqualError(t, err, "Must include 'main'")
	require.Nil(t, got)
}

func TestParentLicense(t *testing.T) {
	v := viper.New()
	v.Set("meta_packages", map[string]interface{}{
		"foo": map[string]interface{}{
			"author": "Antonius Block",
			"main":   "./cli",
		},
	})
	v.Set("license", "MIT")
	got, err := GetMetaPackagesWithViper(v)
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, "MIT", got[0].License)
}
