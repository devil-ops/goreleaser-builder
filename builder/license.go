package builder

import (
	"errors"
	"strings"

	"github.com/go-enry/go-license-detector/v4/licensedb"
	"github.com/rs/zerolog/log"
)

func DetectLicense(dir string) (string, error) {
	log.Warn().Msg("We are auto detecting the license here. This isn't a perfect process, if it detects incorrectly, add it manually to your config with the 'license' config option")
	results := licensedb.Analyse(dir)
	licenses := []string{}
	for _, result := range results {
		if len(result.Matches) == 0 {
			return "", errors.New("could not find any licenses, please specify one in your config")
		} else {
			best := result.Matches[0]
			if best.Confidence < 0.9 {
				log.Warn().Str("best-guess", best.License).Float32("confidense", best.Confidence).Msg("Detected license, but not confident enough to use it, please specify one instead")
				return "", errors.New("not confident enough in license to auto detect it")
			}
			licenses = append(licenses, best.License)
		}
	}
	if len(licenses) == 0 {
		return "", errors.New("could not find any licenses, please specify one in your config")
	}
	return strings.Join(licenses, ","), nil
}
